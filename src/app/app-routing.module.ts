import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FlexComponent } from './flex/flex.component';
import { AppComponent } from './app.component';
import { ComponentAComponent } from './component-a/component-a.component';
import { ComponentBComponent } from './component-b/component-b.component';

const routes: Routes = [ 
  {path:'componenta', component: ComponentAComponent},
  {path:'componentb', component: ComponentBComponent},
  {path: 'flex', component: FlexComponent},  
  //{path: 'home', component:AppComponent},
  //{path:'**', component: Pagenotfoundcomponent}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }


